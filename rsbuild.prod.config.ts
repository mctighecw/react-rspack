import path from "path";
import { pluginReact } from "@rsbuild/plugin-react";

module.exports = {
  source: {
    alias: {
      "@": path.resolve(__dirname, "src"),
      LessConstants: path.resolve(__dirname, "src/styles/constants.less"),
    },
    define: {
      "process.env.ENV_VAR": JSON.stringify("ENV_VAR"),
    },
  },
  output: {
    cssModules: {
      localIdentName: "[hash:8]",
    },
    filename: {
      js: "[contenthash].js",
      css: "[contenthash].css",
      svg: "[contenthash].svg",
      font: "[contenthash][ext]",
      image: "[contenthash][ext]",
      media: "[contenthash][ext]",
    },
    publicPath: "/",
    sourceMap: {
      js: false,
      css: false,
    },
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          {
            loader: "less-loader",
          },
        ],
        type: "css/auto",
      },
    ],
  },
  html: {
    favicon: path.resolve(__dirname, "src/assets/favicon.png"),
    template: path.resolve(__dirname, "src/index.html"),
  },
  plugins: [pluginReact()],
  performance: {
    removeConsole: true,
  },
};
