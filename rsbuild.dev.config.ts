import path from "path";
import { pluginReact } from "@rsbuild/plugin-react";

// See: https://rsbuild.dev/config/index.html

module.exports = {
  source: {
    alias: {
      "@": path.resolve(__dirname, "src"),
      LessConstants: path.resolve(__dirname, "src/styles/constants.less"),
    },
    define: {
      "process.env.ENV_VAR": JSON.stringify("ENV_VAR"),
    },
  },
  output: {
    cssModules: {
      localIdentName: "[hash:8]",
    },
    sourceMap: {
      js: "cheap-module-source-map",
      css: true,
    },
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          {
            loader: "less-loader",
          },
        ],
        type: "css/auto", // to support '*.module.less' as CSS Module
      },
    ],
  },
  html: {
    favicon: path.resolve(__dirname, "src/assets/favicon.png"),
    template: path.resolve(__dirname, "src/index.html"),
  },
  plugins: [pluginReact()],
  server: {
    port: 3000,
    historyApiFallback: true,
  },
};
