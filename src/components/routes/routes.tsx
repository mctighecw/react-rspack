import React from "react";
import { Routes as RouterRoutes, Route, Navigate } from "react-router-dom";
import Main from "@/components/main";

const Routes = () => {
  return (
    <RouterRoutes>
      <Route path="/main" element={<Main />} />
      <Route path="*" element={<Navigate to="/main" replace />} />
    </RouterRoutes>
  );
};

export default Routes;
