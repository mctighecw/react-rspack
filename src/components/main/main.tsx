import React from "react";
import useTheme from "@/hooks/use-theme";
import styles from "./styles.module.less";

const Main = () => {
  const { theme, toggleTheme, getStyles } = useTheme();
  const applyStyles = (className: string) => getStyles(className, styles);

  return (
    <div className={styles.app}>
      <button className={styles.themeButton} onClick={toggleTheme}>
        {theme}
      </button>
      <div className={applyStyles(styles.content)}>
        <div className={applyStyles(styles.text)}>Welcome to React Rspack</div>
      </div>
    </div>
  );
};

export default Main;
