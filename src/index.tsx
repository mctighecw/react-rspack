import React from "react";
import * as ReactDOMClient from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import Routes from "@/components/routes";
import { ThemeProvider } from "@/context/theme";
import "@/styles/global.css";

const container = document.getElementById("root");
const root = ReactDOMClient.createRoot(container);

root.render(
  <ThemeProvider>
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  </ThemeProvider>
);
