# README

Project template for a React app using TypeScript, Less Modules and the [Rspack](https://www.rspack.dev) build tool [Rsbuild](https://rsbuild.dev).

Rspack is similar to Webpack, but more performant for both running the development server and creating a production build.

## App Information

App Name: react-rspack

Created: March 2024

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/react-rspack)

## Tech Stack

- TypeScript
- React
- React Router
- Less Modules
- Prettier
- ESLint
- Rspack/Rsbuild
- Docker

## To Run

_Development_

```sh
$ yarn install
$ yarn start
```

_Production_

Using `docker run`

```sh
$ docker build \
  -f Dockerfile.prod \
  -t react-rspack . \
  && docker run \
  --rm -it \
  --name react-rspack \
  -p 80:80 \
  react-rspack
```
